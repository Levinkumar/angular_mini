import { Component, OnInit } from '@angular/core';
import{FormGroup,FormControl} from '@angular/forms'
import { HotelService } from '../hotel.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edithotel',
  templateUrl: './edithotel.component.html',
  styleUrls: ['./edithotel.component.css']
})
export class EdithotelComponent implements OnInit {

  constructor(private hotel:HotelService,private router:ActivatedRoute) { }
  editHotel=new FormGroup({
    name: new FormControl(''),
    location: new FormControl(''),
    phonenumber: new FormControl('')
  });
  message:boolean=false;
    ngOnInit(): void {
     // console.log(this.router.snapshot.params.id);
      this.hotel.getHotelById(this.router.snapshot.params['id']).subscribe((result: any)=>{
        //console.log(result);
       this.editHotel=new FormGroup({
          name: new FormControl(result['name']),
          location: new FormControl(result['location']),
          phonenumber: new FormControl(result['phonenumber'])
        });
      });
    }
    UpdateData(){
     this.hotel.UpdateHotelData(this.router.snapshot.params['id'],this.editHotel.value).subscribe((result)=>{
      this.message=true;
     });
    }
    removemsg(){
      this.message=false;
    }

}
