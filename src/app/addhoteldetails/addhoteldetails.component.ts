import { Component, OnInit } from '@angular/core';
import{FormGroup,FormControl} from '@angular/forms'
import { HotelService } from '../hotel.service';

@Component({
  selector: 'app-addhoteldetails',
  templateUrl: './addhoteldetails.component.html',
  styleUrls: ['./addhoteldetails.component.css']
})
export class AddhoteldetailsComponent implements OnInit {

  constructor(private hotel:HotelService) { }
 addHotel=new FormGroup({
  name: new FormControl(''),
  location: new FormControl(''),
  phonenumber: new FormControl('')
});
message:boolean=false;
  ngOnInit(): void {
  }
  SaveData(){
    //console.log(this.addHotel.value);
    this.hotel.saveHotelData(this.addHotel.value).subscribe((result)=>{
      //console.log(result)
      this.message=true;
      this.addHotel.reset({});
    });
  }
  removemsg(){
    this.message=false;
  }
  

}
