import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import{FormsModule}from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { MaincontentComponent } from './maincontent/maincontent.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule,Routes } from '@angular/router';
import { HoteldetailsComponent } from './hoteldetails/hoteldetails.component';
import { SignupComponent } from './signup/signup.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { AddhoteldetailsComponent } from './addhoteldetails/addhoteldetails.component';
import { EdithotelComponent } from './edithotel/edithotel.component';
const routes: Routes = [
  {path:'',redirectTo:'home',pathMatch:'full'},
  {path: 'home',component:HomeComponent},
  {path: 'login',component:LoginComponent},
  {path: 'main',component:MaincontentComponent},
  {path:'hoteldetails',component:HoteldetailsComponent},
  {path:'signup',component:SignupComponent},
  {path:'addhotel',component:AddhoteldetailsComponent},
  {path:'edit/:id',component:EdithotelComponent}
  
];
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    MaincontentComponent,
    HoteldetailsComponent,
    SignupComponent,
    AddhoteldetailsComponent,
    EdithotelComponent,
  ],
  imports: [
    BrowserModule,FormsModule,
    AppRoutingModule,HttpClientModule,ReactiveFormsModule,
    BrowserAnimationsModule,RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
