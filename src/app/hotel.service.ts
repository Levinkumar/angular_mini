import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class HotelService {
  url='http://localhost:3000/Hotel'

  constructor(private http:HttpClient) { }
  getAllHotel(){
    return this.http.get(this.url)
  }
saveHotelData(data:any){
 console.log(data);
 return this.http.post(this.url,data);
}
deleteHotel(id: any){
  return this.http.delete(`${this.url}/${id}`);
}
getHotelById(id :number){
  return this.http.get(`${this.url}/${id}`);
}
UpdateHotelData(id:number,data:any){
  return this.http.put(`${this.url}/${id}`,data);
}
}
