import { Component, OnInit } from '@angular/core';
import { HotelService } from '../hotel.service';

@Component({
  selector: 'app-hoteldetails',
  templateUrl: './hoteldetails.component.html',
  styleUrls: ['./hoteldetails.component.css']
})
export class HoteldetailsComponent implements OnInit {

  constructor(private hotel:HotelService) { }
hotelData: any=[];
  ngOnInit(): void {
    this.hotel.getAllHotel().subscribe((allData)=>{
      console.log(allData);
      this.hotelData=allData;
    });
  }
  deleteHotel(id:any){
     this.hotel.deleteHotel(id).subscribe((result)=>{
      //console.log(result);
      this.ngOnInit();
     });
  }

}
